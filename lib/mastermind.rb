class Code
  attr_reader :pegs

  PEGS = { 'r' => :red, 'g' => :green, 'b' => :blue, 'y' => :yellow,
         'o' => :orange, 'p' => :purple }

   def initialize(pegs)
     raise unless pegs.is_a? Array
     @pegs = pegs
   end

   def self.parse(string)
     pegged = string.downcase.split('')
     pegged.each { |x| raise if !(PEGS.keys.include?(x)) }

     Code.new(pegged)
   end

   def self.random
     rand_pegs = []
     4.times { rand_pegs << PEGS.keys[(rand(0..6))] }
     Code.new(rand_pegs)
   end

   def [](index)
     @pegs[index]
   end

   def exact_matches(guesses)
     matches = 0

     (0..3).each do |x|
       matches += 1 if (pegs[x].downcase == guesses.pegs[x].downcase)
     end

     matches
   end

   def near_matches(guesses)
     near_matches = 0
     cloned = @pegs

     guesses.pegs.each_with_index do |x, y|
       if cloned.include?(x) && self[y] != guesses[y]
         near_matches += 1
         cloned.slice!(cloned.index(x))
       end
     end

     near_matches
   end

   def ==(guess)
     return false if !(guess.is_a?(Code))
     exact_matches(guess) == 4
   end
  end

class Game
  attr_accessor :get_guess
  attr_reader :secret_code

  def initialize(secret_code = nil)
    if secret_code.nil?
      @secret_code = Code.random
    else
      @secret_code = secret_code
    end
  end

  def get_guess
    puts "Guess the 4 colors."
    guess = $stdin.gets.chomp
    Code.parse(guess)
  end

  def display_matches(code)
    puts "exact matches: #{secret_code.exact_matches(code)}"
    puts "near matches: #{secret_code.near_matches(code)}"
  end
end
